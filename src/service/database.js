const DB_CONNECTION = process.env.DB_CONNECTION

const MongoClient = require('mongodb').MongoClient
const client = new MongoClient(DB_CONNECTION, { useNewUrlParser: true, useUnifiedTopology: true })

async function addUser (user, guild) {
  return new Promise((resolve, reject) => {
    client.connect(() => {
      const collection = client.db('Discord-Data').collection('Users')
      collection.insertOne({
        user_id: user.id,
        username: user.username,
        discriminator: user.discriminator,
        guilds: [{ guild_id: guild.id, guild_name: guild.name }]
      }).then(() => {
        console.log(`Registered ${user}`)
        resolve(true)
      }).catch(err => {
        console.log(`Error adding user ${user}: ${err}`)
        reject(err)
      })
    })
  })
}

async function addAllUsers (guild) {
  return new Promise((resolve, reject) => {
    const members = Array.from(guild.members.values())
    let total = true
    for (let i = 0; i < members.length; i++) {
      addUser(members[i].user, guild).then((correct) => {
        total = total && correct
      }).catch((err) => {
        reject(err)
      })
    }
    if (total) {
      resolve(total)
    }
  })
}

async function countUsers () {
  return new Promise((resolve, reject) => {
    client.connect(() => {
      const collection = client.db('Discord-Data').collection('Users')
      collection.countDocuments().then((count) => {
        console.log('Counted users')
        resolve(count)
      }).catch(err => {
        console.log('Error counting users')
        reject(err)
      })
    })
  })
}

async function addGuild (guild) {
  return new Promise((resolve, reject) => {
    client.connect(() => {
      const collection = client.db('Discord-Data').collection('Guilds')
      collection.insertOne({
        guild_id: guild.id,
        guild_name: guild.name,
        guild_member_count: guild.memberCount,
        guild_joined_timestamp: guild.joinedTimestamp
      }).then(async () => {
        console.log('done adding server')
        addAllUsers(guild)
          .then((usersAdded) => {
            resolve(usersAdded)
          })
          .catch((err) => {
            reject(err)
          })
      }).catch((err) => {
        console.log(`Error ${err}`)
        reject(err)
      })
    })
  })
}

module.exports = { addUser, countUsers, addGuild }
