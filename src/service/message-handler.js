const Discord = require('discord.js')
const DatabaseService = require('./database')

class MessageHandler {
  processMessage (voice, recv, user) {
    if (this._checkPrefix(recv) && recv.author !== user) {
      const [command, argv] = this._removePrefix(recv)
      switch (command) {
        case 'help':
          this._help(recv, argv)
          break
        case 'ping':
          this._ping(recv, argv)
          break
        case 'join':
        case 'connect':
          voice.connect(recv, argv)
          break
        case 'play':
          voice.play(recv, argv)
          break
        case 'pause':
        case 'stop':
          voice.pause(recv, argv)
          break
        case 'next':
        case 'skip':
          voice.skip(recv, argv)
          break
        case 'resume':
          voice.resume(recv, argv)
          break
        case 'leave':
        case 'disconnect':
          voice.disconnect(recv, argv)
          break
        case 'queue':
          voice.queue(recv)
          break
        case 'np':
        case 'now':
          voice.np(recv, argv)
          break
        case 'clear':
        case 'c':
          voice.clear(recv, argv)
          break
        case 'registerme':
          this._addUser(recv)
          break
        case 'registerserver':
          this._addServer(recv)
          break
        case 'totalusers':
          this._countUsers(recv)
          break
        default:
          recv.channel.send('Danchou... sorry I don\'t understand: ' + recv.content)
      }
    }
  }

  _checkPrefix (recv) {
    return recv.content.startsWith('()')
  }

  _removePrefix (recv) {
    const fullCommand = recv.content.substr(2)
    const splitCommand = fullCommand.split(' ')
    const primaryCommand = splitCommand[0]
    const argv = splitCommand.slice(1)

    console.log(`Command received: ${primaryCommand}`)
    console.log(`Arguments: ${argv}`)
    return [primaryCommand, argv]
  }

  _countUsers (recv) {
    DatabaseService.countUsers()
      .then((count) => {
        if (count > 0) {
          recv.channel.send(`There are ${count} users on my adorable roster!`)
        } else {
          recv.channel.send('Danchou... I had some trouble...')
        }
      })
  }

  _addUser (recv) {
    const user = recv.author
    const guild = recv.guild
    DatabaseService.addUser(user, guild)
      .then((correct) => {
        if (correct) {
          recv.channel.send('You have been added to the roster.')
        } else {
          recv.channel.send('Danchou... I had some trouble...')
        }
      })
  }

  _addServer (recv) {
    DatabaseService.addGuild(recv.guild)
      .then((correct) => {
        if (correct) {
          recv.channel.send('Danchou I have added the whole server to the roster.')
        } else {
          recv.channel.send('Danchou... I had some trouble...')
        }
      })
  }

  _help (recv, argv) {
    const embedded = new Discord.RichEmbed()
      .setColor('#0099ff')
      .setTitle('Help')
      .setDescription('List of bot commands:')
      .addField('Prefix', '()', true)
      .addBlankField()
      .addField('help', 'Gives a list of bot commands')
      .addField('ping', 'Poke Cagliostro to make sure she is awake')
      .addField('connect/join', 'Cagliostro will join the voice channel you are in')
      .addField('disconnect/leave', 'Cagliostro will go conduct more experiments in the meantime')
      .addField('play', 'Cagliostro will play a song for you. If there is already a song playing, it will be put on her todo list')
      .addField('queue', 'Cagliostro will tell you the queue of songs that are to be played')
      .addField('np/now', 'Cagliostro will tell you what is playing right now')
      .addField('skip/next', 'Cagliostro skips to the next song using the power of Ouroboros')
      .addField('clear', 'Cagliostro will destroy the queue with Ars Magna')
      .addBlankField()
      .addField('registerme', 'Cagliostro will add you to the bot.')
      .addField('totalusers', 'Cagliostro will count the entire roster.')
      .addField('registerserver', 'Cagliostro will register your server on her roster.')
    recv.channel.send(embedded)
  }

  _ping (recv, argv) {
    recv.channel.send('pong')
  }
}

module.exports = MessageHandler
