const find = require('lodash.find')
const reject = require('lodash.reject')
const ytdl = require('ytdl-core')
const Discord = require('discord.js')

let voiceConnections = []

class Voice {
  connect (recv, argv) {
    if (recv.member.voiceChannel) {
      return recv.member.voiceChannel.join()
        .then(connection => {
          const data = {
            guild: recv.guild,
            connection: connection,
            queue: [],
            dispatcher: null,
            cargv: null,
            crecv: null
          }
          voiceConnections.push(data)
          return connection
        })
        .catch(console.log)
    } else {
      recv.reply('Danchou is not in a voice channel...')
    }
  }

  play (recv, argv) {
    const entry = find(voiceConnections, { guild: recv.guild })
    if (entry !== undefined) {
      this._playSong(entry.connection, recv, argv)
    } else {
      this.connect(recv, argv)
        .then(connection => {
          this._playSong(connection, recv, argv)
        })
    }
  }

  _playSong (connection, recv, argv) {
    const url = argv[0]
    const entry = find(voiceConnections, { guild: recv.guild })
    if (entry.dispatcher === null) {
      const dispatcher = connection.playStream(ytdl(url, { filter: 'audioonly' }))
        .on('end', () => {
          console.log('Finished playing')
          this._nextSong(recv)
        })
        .on('error', e => {
          console.log(e)
        })
      entry.dispatcher = dispatcher
      entry.crecv = recv
      entry.cargv = argv
    } else {
      entry.queue.push({ recv: recv, argv: argv })
    }
  }

  skip (recv, argv) {
    const entry = find(voiceConnections, { guild: recv.guild })
    if (entry !== undefined && entry.dispatcher !== null) {
      entry.dispatcher.destroy()
    } else {
      recv.reply('Danchou... there is no audio playing...')
    }
  }

  _nextSong (recv) {
    const next = find(voiceConnections, { guild: recv.guild })
    if (next.queue.length > 0) {
      next.dispatcher = null
      next.cargv = null
      next.crecv = null
      const nextSong = next.queue.shift()
      console.log('nextSong')
      if (nextSong !== undefined) {
        recv.channel.send('Playing next song!')
        this._playSong(next.connection, nextSong.recv, nextSong.argv)
      }
    } else {
      next.dispatcher = null
      next.cargv = null
      next.crecv = null
      recv.channel.send('Danchou... there are no more songs in the queue.')
    }
  }

  np (recv) {
    const next = find(voiceConnections, { guild: recv.guild })
    if (next !== undefined && next.dispatcher !== null) {
      const embedded = new Discord.RichEmbed()
        .setColor('#0099ff')
        .setTitle('Now Playing')
        .setDescription(`Requested by ${next.crecv.author}: ${next.cargv[0]}`)
      recv.channel.send(embedded)
    } else {
      recv.channel.send('Danchou... there are no songs playing...')
    }
  }

  queue (recv) {
    const next = find(voiceConnections, { guild: recv.guild })
    const queue = next.queue
    const embedded = new Discord.RichEmbed()
      .setColor('#0099ff')
      .setTitle('Music Queue')
      .addBlankField()

    if (next.dispatcher !== null) {
      embedded.addField('Now Playing', `Requested by ${next.crecv.author}: ${next.cargv[0]}`)
    }

    for (let i = 0; i < queue.length; i++) {
      embedded.addField(`${i + 1}`, `Requested by ${queue[i].recv.author}: ${queue[i].argv[0]}`)
    }

    recv.channel.send(embedded)
  }

  resume (recv, argv) {
    const entry = find(voiceConnections, { guild: recv.guild })
    if (entry !== undefined && entry.dispatcher !== null) {
      entry.dispatcher.resume()
      recv.channel.send('Haiii Danchou! Resumed!')
    } else {
      recv.reply('Danchou... there is no audio paused...')
    }
  }

  pause (recv, argv) {
    const entry = find(voiceConnections, { guild: recv.guild })
    if (entry !== undefined && entry.dispatcher !== null) {
      entry.dispatcher.pause()
      recv.channel.send('Haiii Danchou! Audio paused!')
    } else {
      recv.reply('Danchou... there is no audio playing...')
    }
  }

  clear (recv) {
    const entry = find(voiceConnections, { guild: recv.guild })
    if (entry !== undefined) {
      entry.queue = []
      recv.channel.send('Haiii Danchou! Queue Cleared~<3')
    } else {
      recv.reply('Danchou... there are already no songs in the queue. You would know this if you were as great as the adorable Cagliostro!')
    }
  }

  disconnect (recv, argv) {
    const entry = find(voiceConnections, { guild: recv.guild })
    if (entry !== undefined) {
      entry.connection.disconnect()
      voiceConnections = reject(voiceConnections, { guild: recv.guild })

      recv.reply('Maybe you can help me out another time danchou...')
    } else {
      recv.reply('Danchou... I am not connected to a voice channel.')
    }
  }
}

module.exports = Voice
