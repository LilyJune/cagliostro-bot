require('dotenv').config()
const BOT_SECRET_TOKEN = process.env.BST

const Voice = require('./service/voice')
const voice = new Voice()
const MessageHandler = require('./service/message-handler')
const messageHandler = new MessageHandler()

const Discord = require('discord.js')
const client = new Discord.Client()

client.on('ready', () => {
  console.log(`Connected as ${client.user.tag}`)
})

client.on('message', async recv => {
  const user = recv.author
  if (user !== client.user) {
    if (recv.content.includes('cute')) {
      if (recv.content.includes('<@!663130088451342337>')) {
        recv.channel.send(`${user.toString()}, isn't Cagliostro the cutest?`)
      } else {
        recv.channel.send('Isn\'t Cagliostro the cutest?')
      }
    }
  }

  messageHandler.processMessage(voice, recv, client.user)
})

client.login(BOT_SECRET_TOKEN)
